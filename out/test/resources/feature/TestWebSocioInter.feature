@Internacional
Feature: Validar fluxos de dados para incluir sócio do Internacional

  @RealizarCadastroCompletoPositivo
  Scenario: Realizar Cadastro para Socio
    Given que fiz acesso para cadastro completo
    When inserir dados
    Then valido a insercao

  @ExcessoesDadosAcessoNome
  Scenario: Validar excessoes dados de acesso nome
    Given que acessei o site
    When inserir nome incompleto
    Then testar mensagem

  @ExcessoesDadosAcessoCPF
  Scenario: Validar excessoes dados de acesso CPF
    Given que acessei o site
    When inserir cpf invalido e incompleto
    Then valido msg CPF invalido

  @ExcessoesDadosAcessoDataNascimento
  Scenario: Validar excessoes dados de acesso data de nascimento
    Given que acessei o site
    When inserir data nascimento incompleta e formato invalido
    Then valido msg Data invalida

  @ExcessoesDadosAcessoCelular
  Scenario: Validar excessoes dados de acesso celular
    Given que acessei o site
    When inserir celular sem DDD e celular vazio
    Then valido msg Celular obrigatorio.

  @ExcessoesDadosAcessoSenha
  Scenario: Validar excessoes dados de acesso senha
    Given que acessei o site
    When inserir senha de letras e senha de quatro digitos
    Then valido msg Senhas devem ser iguais

  @ExcessoesConfirmacaoCepIncompleto
  Scenario: Validar excessoes confirmacao cep incompleto
    Given que preenchi dados de acesso
    When inserir cep incompleto
    Then valido msg CEP obrigatorio

  @ExcessoesConfirmacaoCepInvalido
  Scenario: Validar excessoes confirmacao cep invalido
    Given que preenchi dados de acesso
    When enviar valor invalido
    Then valido msg nao foi possivel localizar endereco

  @ExcessoesConfirmacaoRua
  Scenario: Validar excessoes confirmacao rua
    Given que preenchi dados de acesso
    When alterar nome da rua
    Then valido msg

  @ExcessoesConfirmacaoNumeroCasa
  Scenario: Validar excessoes confirmacao dados pagamento
    Given que preenchi dados de acesso
    When não preencher numero da casa
    Then validar numero obrigatorio

  @ExcessoesConfirmacaoPreenchimentoAutomatico
  Scenario: Validar excessoes confirmacao preenchimento automatico
    Given que preenchi dados de acesso
    When incluir cep
    Then valido dados preenchidos automaticamente

  @ExcessoesConfirmacaoDadosPagamento
  Scenario: Validar excessoes confirmacao dados pagamento
    Given que preenchi dados de acesso
    When enviar dados da agencia bancaria
    Then valido