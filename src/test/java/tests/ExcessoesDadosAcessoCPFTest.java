package tests;

import cucumber.api.java.After;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import request.CadastroCompletoRequest;
import request.DadosAcessoRequest;

import java.util.ArrayList;

public class ExcessoesDadosAcessoCPFTest {

    DadosAcessoRequest dadosAcessoRequest = new DadosAcessoRequest();
    public static WebDriver navegador;

    @When("inserir cpf invalido e incompleto")
    public void inserir_cpf_invalido_e_incompleto() throws Throwable {
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#gatsby-focus-wrapper > main > article > section.associe-se__BannerWrapper-b51u4m-1.eFjOge > a > figure > img")).click();
        Thread.sleep(2000);

        ArrayList<String> tabs2 = new ArrayList<String> (dadosAcessoRequest.navegador.getWindowHandles());
        dadosAcessoRequest.navegador.switchTo().window(tabs2.get(1));
        Thread.sleep(2000);
        dadosAcessoRequest.navegador.findElement(By.id("firstName")).sendKeys("Teste Web");
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(1) > div > div:nth-child(2) > div > div:nth-child(2) > select")).sendKeys("F");
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(1) > div > div:nth-child(2) > div > div:nth-child(2) > select")).click();

        // inserir CPF incompleto
        dadosAcessoRequest.navegador.findElement(By.id("cpf")).sendKeys("");
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(2) > div > div:nth-child(2) > div > div:nth-child(2) > datepicker > input")).sendKeys("1010s2000");

        //inserir CPF inválido
        dadosAcessoRequest.navegador.findElement(By.id("cpf")).sendKeys("00000000000");
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(2) > div > div:nth-child(2) > div > div:nth-child(2) > datepicker > input")).sendKeys("1010s2000");
    }

    @Then("valido msg CPF invalido")
    public void valido_msg_CPF_invalido() throws Throwable {
        //CPF Incompleto não gera msg de erro
        String msgErro = dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(2) > div > div:nth-child(1) > div:nth-child(1) > div.col-md-8.col-form > div > p")).getText();
        Assert.assertEquals(msgErro, "CPF inválido.");

        dadosAcessoRequest.navegador.quit();
    }
}
