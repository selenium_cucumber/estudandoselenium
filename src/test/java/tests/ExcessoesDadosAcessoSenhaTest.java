package tests;

import cucumber.api.java.After;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import request.CadastroCompletoRequest;
import request.DadosAcessoRequest;

import java.util.ArrayList;

public class ExcessoesDadosAcessoSenhaTest {

    DadosAcessoRequest dadosAcessoRequest = new DadosAcessoRequest();
    public static WebDriver navegador;

    @When("inserir senha de letras e senha de quatro digitos")
    public void inserir_senha_de_letras_e_senha_de_quatro_digitos() throws Throwable {
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#gatsby-focus-wrapper > main > article > section.associe-se__BannerWrapper-b51u4m-1.eFjOge > a > figure > img")).click();
        Thread.sleep(2000);

        ArrayList<String> tabs2 = new ArrayList<String> (dadosAcessoRequest.navegador.getWindowHandles());
        dadosAcessoRequest.navegador.switchTo().window(tabs2.get(1));
        Thread.sleep(2000);
        dadosAcessoRequest.navegador.findElement(By.id("firstName")).sendKeys("Teste Web");
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(1) > div > div:nth-child(2) > div > div:nth-child(2) > select")).sendKeys("F");
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(1) > div > div:nth-child(2) > div > div:nth-child(2) > select")).click();
        dadosAcessoRequest.navegador.findElement(By.id("cpf")).sendKeys("00381744051");
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(2) > div > div:nth-child(2) > div > div:nth-child(2) > datepicker > input")).sendKeys("10102000");
        dadosAcessoRequest.navegador.findElement(By.id("email")).sendKeys("testeweb@teste.com");
        dadosAcessoRequest.navegador.findElement(By.id("tel")).sendKeys("51999988776");
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(5) > div > div > div > div:nth-child(1) > div > div.col-md-8.col-form > input-senha > input-senha-139 > div > input")).sendKeys("senhas");
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(5) > div > div > div > div:nth-child(2) > div > div.col-md-8.col-form > input-senha > input-senha-139 > div > input")).sendKeys("senhas");
        dadosAcessoRequest.navegador.findElement(By.id("btn-avancar-pagina")).click();
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(5) > div > div > div > div:nth-child(1) > div > div.col-md-8.col-form > input-senha > input-senha-139 > div > input")).sendKeys("698315");
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(5) > div > div > div > div:nth-child(2) > div > div.col-md-8.col-form > input-senha > input-senha-139 > div > input")).sendKeys("698316");
        dadosAcessoRequest.navegador.findElement(By.id("btn-avancar-pagina")).click();
    }

    @Then("valido msg Senhas devem ser iguais")
    public void valido_msg_senhas_devem_ser_iguais() throws Throwable {
    System.out.println("Quando inserido senha com letras, ou com menos de 6 digitos, o sistema não envia mensagem de erro mas não permite a entrada dos dados e não habilita o botão Avançar!");
    //validar senhas diferentes
        String msgErro = dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(5) > div > div > div > div:nth-child(2) > div > div.col-md-8.col-form > div > p")).getText();
        Assert.assertEquals(msgErro, "Senhas devem ser iguais.");

        dadosAcessoRequest.navegador.quit();
    }
}
