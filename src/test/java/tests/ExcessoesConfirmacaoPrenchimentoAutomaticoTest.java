package tests;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import request.ConfirmacaoRequest;

public class ExcessoesConfirmacaoPrenchimentoAutomaticoTest {

    ConfirmacaoRequest confirmacaoRequest = new ConfirmacaoRequest();
    public static WebDriver navegador;

    @When("incluir cep")
    public void incluir_cep() throws Throwable {
        confirmacaoRequest.navegador.findElement(By.cssSelector("#Etapa02UI > section > div > div > form > form-set-endereco > div > ng-form > div:nth-child(1) > div.etapa2-cep > div.col-form > div.d-inline-flex > input")).sendKeys("93212400");
        Thread.sleep(2000);
    }

    @Then("valido dados preenchidos automaticamente")
    public void valido_dados_preenchidos_automaticamente() throws Throwable {
        //validar País
        String pais = confirmacaoRequest.navegador.findElement(By.cssSelector("#Etapa02UI > section > div > div > form > form-set-endereco > div > ng-form > div:nth-child(1) > div.etapa2-pais > div:nth-child(2) > select > option:nth-child(26)")).getText();
        Assert.assertEquals(pais, "Brasil");

        //Validar Estado
        String estado = confirmacaoRequest.navegador.findElement(By.cssSelector("#Etapa02UI > section > div > div > form > form-set-endereco > div > ng-form > div:nth-child(2) > div.etapa2-estado > div.d-inline-flex > select > option:nth-child(21)")).getText();
        Assert.assertEquals(estado, "Rio Grande do Sul");
    }
}
