package tests;

import cucumber.api.java.After;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import request.CadastroCompletoRequest;
import request.DadosAcessoRequest;

import java.util.ArrayList;

public class ExcessoesDadosAcessoDataNascTest {

    DadosAcessoRequest dadosAcessoRequest = new DadosAcessoRequest();
    public static WebDriver navegador;

    @When("inserir data nascimento incompleta e formato invalido")
    public void inserir_data_nascimento_incompleta_e_formato_invalido() throws Throwable {

        dadosAcessoRequest.navegador.findElement(By.cssSelector("#gatsby-focus-wrapper > main > article > section.associe-se__BannerWrapper-b51u4m-1.eFjOge > a > figure > img")).click();
        Thread.sleep(2000);

        ArrayList<String> tabs2 = new ArrayList<String> (dadosAcessoRequest.navegador.getWindowHandles());
        dadosAcessoRequest.navegador.switchTo().window(tabs2.get(1));
        Thread.sleep(2000);
        dadosAcessoRequest.navegador.findElement(By.id("firstName")).sendKeys("Teste Web");
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(1) > div > div:nth-child(2) > div > div:nth-child(2) > select")).sendKeys("F");
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(1) > div > div:nth-child(2) > div > div:nth-child(2) > select")).click();
        dadosAcessoRequest.navegador.findElement(By.id("cpf")).sendKeys("00381744051");
        //data nascimento
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(2) > div > div:nth-child(2) > div > div:nth-child(2) > datepicker > input")).sendKeys("");
        dadosAcessoRequest.navegador.findElement(By.id("email")).sendKeys("testeweb@teste.com");

    }

    @Then("valido msg Data invalida")
    public void valido_msg_Data_invalida() throws Throwable {
       System.out.println("Para data de nascimento digitada como 10-10-2000 o sistema aceita sem mensagem de erro");
        String msgErro = dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(2) > div > div:nth-child(2) > div > div:nth-child(2) > div > p")).getText();
        Assert.assertEquals(msgErro, "Data inválida.");

        dadosAcessoRequest.navegador.quit();
    }
}
