package tests;

import cucumber.api.java.After;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import request.ConfirmacaoRequest;
import request.DadosAcessoRequest;

public class ExcessoesConfirmacaoCepTest {

    ConfirmacaoRequest confirmacaoRequest = new ConfirmacaoRequest();
    public static WebDriver navegador;

    @When("inserir cep incompleto")
    public void inserir_cep_incompleto() throws Throwable {
        confirmacaoRequest.navegador.findElement(By.cssSelector("#Etapa02UI > section > div > div > form > form-set-endereco > div > ng-form > div:nth-child(1) > div.etapa2-cep > div.col-form > div.d-inline-flex > input")).sendKeys("93212");
        Thread.sleep(2000);
        confirmacaoRequest.navegador.findElement(By.cssSelector("#Etapa02UI > section > div > div > form > form-set-endereco > div > ng-form > div:nth-child(2) > div.etapa2-estado > div.d-inline-flex > select")).click();

    }

    @Then("valido msg CEP obrigatorio")
    public void valido_msg_CEP_obrigatorio() throws Throwable {
        String msgErro = confirmacaoRequest.navegador.findElement(By.cssSelector("#Etapa02UI > section > div > div > form > form-set-endereco > div > ng-form > div:nth-child(1) > div.etapa2-cep > div.col-form > div.has-error.input-validation.ng-active > p")).getText();
        Assert.assertEquals(msgErro, "CEP obrigatório.");
        confirmacaoRequest.navegador.quit();
    }

    @When("enviar valor invalido")
    public void enviar_valor_invalido() throws Throwable {
        confirmacaoRequest.navegador.findElement(By.cssSelector("#Etapa02UI > section > div > div > form > form-set-endereco > div > ng-form > div:nth-child(1) > div.etapa2-cep > div.col-form > div.d-inline-flex > input")).sendKeys("00000000");
        Thread.sleep(2000);
    }

    @Then("valido msg nao foi possivel localizar endereco")
    public void valido_msg_nao_foi_possivel_localizar_endereco() throws Throwable {
      String msgErro = confirmacaoRequest.navegador.findElement(By.cssSelector("#Etapa02UI > section > div > div > form > form-set-endereco > div > ng-form > div.error-alert-tip.m-l-0.m-b-25.cursor-pointer")).getText();
      Assert.assertEquals(msgErro, "Não foi possível localizar o endereço, preencha manualmente os campos.");
    }
}
