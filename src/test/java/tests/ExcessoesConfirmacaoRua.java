package tests;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.WebDriver;

import request.ConfirmacaoRequest;

public class ExcessoesConfirmacaoRua {


    ConfirmacaoRequest confirmacaoRequest = new ConfirmacaoRequest();
    public static WebDriver navegador;


    @When("alterar nome da rua")
    public void alterar_nome_da_rua() throws Throwable {
        confirmacaoRequest.navegador.findElement(By.cssSelector("#Etapa02UI > section > div > div > form > form-set-endereco > div > ng-form > div:nth-child(1) > div.etapa2-cep > div.col-form > div.d-inline-flex > input")).sendKeys("93212400");
        Thread.sleep(2000);
    }

    @Then("valido msg")
    public void valido_msg() throws Throwable {
        try{
            confirmacaoRequest.navegador.findElement(By.cssSelector("#Etapa02UI > section > div > div > form > form-set-endereco > div > ng-form > div:nth-child(3) > div.etapa2-rua > div.d-inline-flex > input")).sendKeys("Teste");
        } catch(Exception erro){
            System.out.println("erro: " + erro);
            Assert.assertTrue(String.valueOf(erro).contains("org.openqa.selenium.ElementNotInteractableException: element not interactable"));
        }
            confirmacaoRequest.navegador.quit();
    }

    @When("não preencher numero da casa")
    public void não_preencher_numero_da_casa() throws Throwable {
        confirmacaoRequest.navegador.findElement(By.cssSelector("#Etapa02UI > section > div > div > form > form-set-endereco > div > ng-form > div:nth-child(1) > div.etapa2-cep > div.col-form > div.d-inline-flex > input")).sendKeys("93212400");
        Thread.sleep(2000);
        confirmacaoRequest.navegador.findElement(By.cssSelector("#Etapa02UI > section > div > div > form > form-set-endereco > div > ng-form > div:nth-child(3) > div.etapa2-numero > div.col-form > input")).sendKeys("");
        confirmacaoRequest.navegador.findElement(By.cssSelector("#Etapa02UI > section > div > div > form > form-set-endereco > div > ng-form > div:nth-child(3) > div.etapa2-complemento > div:nth-child(2) > input")).click();

    }

    @Then("validar numero obrigatorio")
    public void validar_numero_obrigatorio() throws Throwable {
        String msgErro = confirmacaoRequest.navegador.findElement(By.cssSelector("#Etapa02UI > section > div > div > form > form-set-endereco > div > ng-form > div:nth-child(3) > div.etapa2-numero > div.col-form > div > p")).getText();
        Assert.assertEquals(msgErro, "Número obrigatório.");

        confirmacaoRequest.navegador.quit();
    }
}
