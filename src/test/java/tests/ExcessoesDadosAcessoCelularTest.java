package tests;

import cucumber.api.java.After;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import request.CadastroCompletoRequest;
import request.DadosAcessoRequest;

import java.util.ArrayList;

public class ExcessoesDadosAcessoCelularTest {

    DadosAcessoRequest dadosAcessoRequest = new DadosAcessoRequest();
    public static WebDriver navegador;

    @When("inserir celular sem DDD e celular vazio")
    public void inserir_celular_sem_DDD_e_celular_vazio() throws Throwable {
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#gatsby-focus-wrapper > main > article > section.associe-se__BannerWrapper-b51u4m-1.eFjOge > a > figure > img")).click();
        Thread.sleep(2000);

        ArrayList<String> tabs2 = new ArrayList<String> (dadosAcessoRequest.navegador.getWindowHandles());
        dadosAcessoRequest.navegador.switchTo().window(tabs2.get(1));
        Thread.sleep(2000);
        dadosAcessoRequest.navegador.findElement(By.id("firstName")).sendKeys("Teste Web");
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(1) > div > div:nth-child(2) > div > div:nth-child(2) > select")).sendKeys("F");
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(1) > div > div:nth-child(2) > div > div:nth-child(2) > select")).click();
        dadosAcessoRequest.navegador.findElement(By.id("cpf")).sendKeys("00381744051");
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(2) > div > div:nth-child(2) > div > div:nth-child(2) > datepicker > input")).sendKeys("10102000");
        dadosAcessoRequest.navegador.findElement(By.id("email")).sendKeys("testeweb@teste.com");
        dadosAcessoRequest.navegador.findElement(By.id("tel")).sendKeys("999999999");
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(5) > div > div > div > div:nth-child(1) > div > div.col-md-8.col-form > input-senha > input-senha-139 > div > input")).sendKeys("597824");
        dadosAcessoRequest.navegador.findElement(By.id("tel")).clear();
        dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(5) > div > div > div > div:nth-child(1) > div > div.col-md-8.col-form > input-senha > input-senha-139 > div > input")).click();
    }

    @Then("valido msg Celular obrigatorio")
    public void valido_msg_Celular_obrigatorio() throws Throwable {
        String msgErro = dadosAcessoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div.form-group.form-group-inline.m-t-15 > div > div:nth-child(2) > div > div:nth-child(2) > div > p")).getText();
        Assert.assertEquals(msgErro, "Celular obrigatório.");
        dadosAcessoRequest.navegador.quit();
    }
}
