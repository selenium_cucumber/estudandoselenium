package tests;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import request.ConfirmacaoRequest;

public class ExcessoesConfimacaoDadosPagamento {

    ConfirmacaoRequest confirmacaoRequest = new ConfirmacaoRequest();
    public static WebDriver navegador;

    @When("enviar dados da agencia bancaria")
    public void enviar_dados_da_agencia_bancaria() throws Throwable {
        confirmacaoRequest.navegador.findElement(By.cssSelector("#Etapa02UI > section > div > div > form > form-set-endereco > div > ng-form > div:nth-child(1) > div.etapa2-cep > div.col-form > div.d-inline-flex > input")).sendKeys("93212400");
        Thread.sleep(2000);
        confirmacaoRequest.navegador.findElement(By.cssSelector("#Etapa02UI > section > div > div > form > form-set-endereco > div > ng-form > div:nth-child(3) > div.etapa2-numero > div.col-form > input")).sendKeys("59");
        confirmacaoRequest.navegador.findElement(By.cssSelector("#Etapa02UI > section > div > div > form > form-set-endereco > div > ng-form > div:nth-child(3) > div.etapa2-complemento > div:nth-child(2) > input")).sendKeys("Apto 600");
        confirmacaoRequest.navegador.findElement(By.cssSelector("#RecorrenteDebitoSection > article > div > div > div:nth-child(2) > label")).click();
        confirmacaoRequest.navegador.findElement(By.cssSelector("#RecorrenteDebitoSection > article > div > form > div.row.m-t-15 > div > div:nth-child(2) > select")).sendKeys("i");
        confirmacaoRequest.navegador.findElement(By.cssSelector("#RecorrenteDebitoSection > article > div > form > div:nth-child(2) > div.Agencia.col-xs-12.col-sm-6.col-md-4.col-lg-4.col-xl-3.m-t-10 > div:nth-child(2) > input")).sendKeys("0");
        confirmacaoRequest.navegador.findElement(By.cssSelector("#RecorrenteDebitoSection > article > div > form > div:nth-child(2) > div.Conta.col-xs-12.col-sm-6.col-md-4.col-lg-4.col-xl-3.m-t-10 > div:nth-child(2) > input")).sendKeys("0");
        confirmacaoRequest.navegador.findElement(By.cssSelector("#RecorrenteDebitoSection > article > div > form > div:nth-child(4) > div.NomeTitular.col-xs-12.col-sm-6.col-md-4.col-lg-4.col-xl-3.m-t-10 > div:nth-child(2) > input")).sendKeys("TESTE");
        confirmacaoRequest.navegador.findElement(By.cssSelector("#RecorrenteDebitoSection > article > div > form > div:nth-child(4) > div.CPFTitular.col-xs-12.col-sm-6.col-md-4.col-lg-4.col-xl-3.m-t-10 > div:nth-child(2) > input")).sendKeys("00000000000");
    }

    @Then("valido")
    public void valido() throws Throwable {
        System.out.println("Sistema permite entrada vazia dos dados bancários, validando apenas após clicar no botão");
    }
}
