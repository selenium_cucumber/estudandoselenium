package tests;

import cucumber.api.java.After;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import request.CadastroCompletoRequest;

import java.util.ArrayList;

public class CadastroCompletoPositivoTest {

    CadastroCompletoRequest cadastroCompletoRequest = new CadastroCompletoRequest();
    public static WebDriver navegador;

    @When("inserir dados")
    public void inserir_dados() throws Throwable {
        cadastroCompletoRequest.navegador.findElement(By.cssSelector("#gatsby-focus-wrapper > main > article > section.associe-se__BannerWrapper-b51u4m-1.eFjOge > a > figure > img")).click();
        Thread.sleep(2000);

        ArrayList<String> tabs2 = new ArrayList<String> (cadastroCompletoRequest.navegador.getWindowHandles());
        cadastroCompletoRequest.navegador.switchTo().window(tabs2.get(1));
        Thread.sleep(2000);
        cadastroCompletoRequest.navegador.findElement(By.id("firstName")).sendKeys("Teste Web");
        cadastroCompletoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(1) > div > div:nth-child(2) > div > div:nth-child(2) > select")).sendKeys("F");
        cadastroCompletoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(1) > div > div:nth-child(2) > div > div:nth-child(2) > select")).click();
        cadastroCompletoRequest.navegador.findElement(By.id("cpf")).sendKeys("00381744051");
        cadastroCompletoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(2) > div > div:nth-child(2) > div > div:nth-child(2) > datepicker > input")).sendKeys("10102000");
        cadastroCompletoRequest.navegador.findElement(By.id("email")).sendKeys("testeweb@teste.com");
        cadastroCompletoRequest.navegador.findElement(By.id("tel")).sendKeys("51999988776");
        cadastroCompletoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(5) > div > div > div > div:nth-child(1) > div > div.col-md-8.col-form > input-senha > input-senha-139 > div > input")).sendKeys("597824");
        cadastroCompletoRequest.navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(5) > div > div > div > div:nth-child(2) > div > div.col-md-8.col-form > input-senha > input-senha-139 > div > input")).sendKeys("597824");
        cadastroCompletoRequest.navegador.findElement(By.id("btn-avancar-pagina")).click();

        cadastroCompletoRequest.navegador.findElement(By.cssSelector("#Etapa02UI > section > div > div > form > form-set-endereco > div > ng-form > div:nth-child(1) > div.etapa2-cep > div.col-form > div.d-inline-flex > input")).sendKeys("93212400");
        Thread.sleep(2000);
        cadastroCompletoRequest.navegador.findElement(By.cssSelector("#Etapa02UI > section > div > div > form > form-set-endereco > div > ng-form > div:nth-child(3) > div.etapa2-numero > div.col-form > input")).sendKeys("59");
        cadastroCompletoRequest.navegador.findElement(By.cssSelector("#Etapa02UI > section > div > div > form > form-set-endereco > div > ng-form > div:nth-child(3) > div.etapa2-complemento > div:nth-child(2) > input")).sendKeys("Apto 600");
        cadastroCompletoRequest.navegador.findElement(By.cssSelector("#RecorrenteDebitoSection > article > div > div > div:nth-child(2) > label")).click();
        cadastroCompletoRequest.navegador.findElement(By.cssSelector("#RecorrenteDebitoSection > article > div > form > div.row.m-t-15 > div > div:nth-child(2) > select")).sendKeys("i");
        cadastroCompletoRequest.navegador.findElement(By.cssSelector("#RecorrenteDebitoSection > article > div > form > div:nth-child(2) > div.Agencia.col-xs-12.col-sm-6.col-md-4.col-lg-4.col-xl-3.m-t-10 > div:nth-child(2) > input")).sendKeys("2890");
        cadastroCompletoRequest.navegador.findElement(By.cssSelector("#RecorrenteDebitoSection > article > div > form > div:nth-child(2) > div.Conta.col-xs-12.col-sm-6.col-md-4.col-lg-4.col-xl-3.m-t-10 > div:nth-child(2) > input")).sendKeys("999807");
        cadastroCompletoRequest.navegador.findElement(By.cssSelector("#RecorrenteDebitoSection > article > div > form > div:nth-child(4) > div.NomeTitular.col-xs-12.col-sm-6.col-md-4.col-lg-4.col-xl-3.m-t-10 > div:nth-child(2) > input")).sendKeys("TESTE WEB");
        cadastroCompletoRequest.navegador.findElement(By.cssSelector("#RecorrenteDebitoSection > article > div > form > div:nth-child(4) > div.CPFTitular.col-xs-12.col-sm-6.col-md-4.col-lg-4.col-xl-3.m-t-10 > div:nth-child(2) > input")).sendKeys("00381744051");
      }

    @Then("valido a insercao")
    public void valido_a_insercao() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        cadastroCompletoRequest.navegador.quit();
    }

}
