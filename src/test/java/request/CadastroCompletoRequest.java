package request;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CadastroCompletoRequest {

    public static WebDriver navegador;

    @Given("que fiz acesso para cadastro completo")
    public void que_fiz_acesso_para_cadastro_completo() throws Throwable {
        System.setProperty("webdriver.chrome.driver", "src/driver/chromedriver.exe");
        navegador = new ChromeDriver();
        navegador.manage().window().maximize();
        navegador.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        navegador.get("https://internacional.com.br/associe-se");
    }

}


