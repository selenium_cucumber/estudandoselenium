package request;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class DadosAcessoRequest {

    public static WebDriver navegador;

    @Given("que acessei o site")
    public void que_acessei_o_site() throws Throwable {

    System.setProperty("webdriver.chrome.driver", "src/driver/chromedriver.exe");
        navegador = new ChromeDriver();
        navegador.manage().window().maximize();
        navegador.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        navegador.get("https://internacional.com.br/associe-se");
    }


}
