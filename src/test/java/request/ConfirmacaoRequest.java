package request;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class ConfirmacaoRequest {


    public static WebDriver navegador;
    public static WebDriver tabs2;

    @Given("que preenchi dados de acesso")
    public void que_preenchi_dados_de_acesso() throws Throwable {
        //acesso
        System.setProperty("webdriver.chrome.driver", "src/driver/chromedriver.exe");
        navegador = new ChromeDriver();
        navegador.manage().window().maximize();
        navegador.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        navegador.get("https://internacional.com.br/associe-se");

        //pagina inicial preenchida

        navegador.findElement(By.cssSelector("#gatsby-focus-wrapper > main > article > section.associe-se__BannerWrapper-b51u4m-1.eFjOge > a > figure > img")).click();
        Thread.sleep(5000);

        ArrayList<String> tabs2 = new ArrayList<String> (navegador.getWindowHandles());
        navegador.switchTo().window(tabs2.get(1));
        Thread.sleep(5000);
        navegador.findElement(By.id("firstName")).sendKeys("Teste Web");
        navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(1) > div > div:nth-child(2) > div > div:nth-child(2) > select")).sendKeys("F");
        navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(1) > div > div:nth-child(2) > div > div:nth-child(2) > select")).click();
        navegador.findElement(By.id("cpf")).sendKeys("00381744051");
        navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(3) > div > div:nth-child(2) > div > div:nth-child(2) > div > div:nth-child(2) > datepicker > input")).sendKeys("10102000");
        navegador.findElement(By.id("email")).sendKeys("testeweb@teste.com");
        navegador.findElement(By.id("tel")).sendKeys("51999988776");
        navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(5) > div > div > div > div:nth-child(1) > div > div.col-md-8.col-form > input-senha > input-senha-139 > div > input")).sendKeys("597824");
        navegador.findElement(By.cssSelector("#Etapa01UI > section > div > div > form > div:nth-child(5) > div > div > div > div:nth-child(2) > div > div.col-md-8.col-form > input-senha > input-senha-139 > div > input")).sendKeys("597824");
        navegador.findElement(By.id("btn-avancar-pagina")).click();
    }

}
